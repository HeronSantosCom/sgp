�
 TCADASTROFORM 0�'  TPF0TCadastroFormCadastroFormLeftTopkBorderIcons BorderStylebsDialogCaptionCadastramentroClientHeight�ClientWidthsColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style OldCreateOrder	PositionpoScreenCenterPixelsPerInch`
TextHeight TPanelPanel1Left Top WidthsHeight�AlignalClient
BevelOuterbvNoneBorderWidthParentColor	TabOrder  TPageControlPageControl1LeftTopWidthiHeight�
ActivePage
TabTalentoAlignalClientTabOrder  	TTabSheet
TabTalentoCaptionCa�a-TalentoOnShowTabTalentoShow TLabelLabel1LeftTopWidth#HeightCaptionNome:FocusControlDBEdit1  TLabelLabel2LeftTop(Width� HeightCaptionData de Nascimento:FocusControlDBEdit2  TLabelLabel3LeftTopHWidthTHeightCaptionCPF ou CNPJ:FocusControlDBEdit3  TLabelLabel4LeftTophWidth[HeightCaptionRaz�o Social:FocusControlDBEdit4  TLabelLabel5LeftTop� WidthMHeightCaptionIdentidade:FocusControlDBEdit5  TLabelLabel6Left� Top� Width~HeightCaptionData de expedi��o:FocusControlDBEdit6  TLabelLabel7LeftTop� WidthpHeightCaptionOrg�o expeditor:FocusControlDBEdit7  TLabelLabel8LeftTop� Width?HeightCaption	Endere�o:FocusControlDBEdit8  TLabelLabel9LeftTop� WidthFHeightCaption
Cidade/UF:FocusControlDBEdit9  TLabelLabel10Left�Top� WidthHeightCaptionCEP:FocusControlDBEdit10  TLabelLabel11LeftTopWidth#HeightCaptionPa�s:FocusControlDBEdit11  TLabelLabel12LeftTop(Width?HeightCaption	Telefone:FocusControlDBEdit12  TLabelLabel13Left� Top(Width8HeightCaptionCelular:FocusControlDBEdit13  TLabelLabel14LeftTopHWidth1HeightCaptionE-mail:FocusControlDBEdit14  TDBEditDBEdit1Left0TopWidth)HeightAutoSize	DataFieldNome
DataSourceDM1.DS2TabOrder 
OnKeyPressDBEdit1KeyPress  TDBEditDBEdit2Left� Top&WidthQHeightAutoSize	DataFieldData de nascimento
DataSourceDM1.DS2	MaxLength
TabOrder  TDBEditDBEdit3Left`TopFWidth� HeightAutoSize	DataFieldCPF ou CNPJ
DataSourceDM1.DS2	MaxLengthTabOrder  TDBEditDBEdit4LefthTopfWidth�HeightAutoSize	DataFieldRaz�o social
DataSourceDM1.DS2TabOrder  TDBEditDBEdit5LeftXTop� WidthJHeightAutoSize	DataField
Identidade
DataSourceDM1.DS2	MaxLengthTabOrder  TDBEditDBEdit6LeftHTop� WidthqHeightAutoSize	DataFieldData de expedi��o
DataSourceDM1.DS2	MaxLength
TabOrder  TDBEditDBEdit7LeftxTop� Width�HeightAutoSize	DataFieldOrg�o expeditor
DataSourceDM1.DS2TabOrder  TDBEditDBEdit8LeftHTop� WidthHeightAutoSize	DataFieldEndere�o
DataSourceDM1.DS2TabOrder  TDBEditDBEdit9LeftPTop� WidthiHeightAutoSize	DataField	Cidade/UF
DataSourceDM1.DS2TabOrder  TDBEditDBEdit10Left�Top� WidthaHeightAutoSize	DataFieldCEP
DataSourceDM1.DS2	MaxLengthTabOrder	  TDBEditDBEdit11Left0TopWidth)HeightAutoSize	DataFieldPa�s
DataSourceDM1.DS2	MaxLength� TabOrder
  TDBEditDBEdit12LeftPTop&WidthJHeightAutoSize	DataFieldTelefone
DataSourceDM1.DS2	MaxLengthTabOrder  TDBEditDBEdit13Left Top&WidthJHeightAutoSize	DataFieldCelular
DataSourceDM1.DS2	MaxLengthTabOrder  TDBEditDBEdit14Left@TopFWidthHeightAutoSize	DataFieldE-mail
DataSourceDM1.DS2TabOrder   	TTabSheet
TabClienteCaptionClienteOnShowTabClienteShow TLabelLabel16LeftTopWidth#HeightCaptionNome:FocusControlDBEdit16  TLabelLabel17LeftTop(Width� HeightCaptionData de Nascimento:FocusControlDBEdit17  TLabelLabel18LeftTopHWidthTHeightCaptionCPF ou CNPJ:FocusControlDBEdit18  TLabelLabel19LeftTophWidthMHeightCaptionIdentidade:FocusControlDBEdit19  TLabelLabel20Left� TophWidth~HeightCaptionData de Expedi��o:FocusControlDBEdit20  TLabelLabel21LeftTop� WidthpHeightCaptionOrg�o Expeditor:FocusControlDBEdit21  TLabelLabel22LeftTop� Width?HeightCaption	Endere�o:FocusControlDBEdit22  TLabelLabel23LeftTop� WidthFHeightCaption
Cidade/UF:FocusControlDBEdit23  TLabelLabel24Left�Top� WidthHeightCaptionCEP:FocusControlDBEdit24  TLabelLabel25LeftTop� Width#HeightCaptionPa�s:FocusControlDBEdit25  TLabelLabel26LeftTopWidth?HeightCaption	Telefone:FocusControlDBEdit26  TLabelLabel27Left� TopWidth8HeightCaptionCelular:FocusControlDBEdit27  TLabelLabel28LeftTop(Width1HeightCaptionE-mail:FocusControlDBEdit28  TLabelLabel29LeftTopHWidth1HeightCaptionPerfil:FocusControlDBMemo1  TDBEditDBEdit16Left0TopWidth)HeightAutoSize	DataFieldNome
DataSourceDM1.DS3TabOrder 
OnKeyPressDBEdit16KeyPress  TDBEditDBEdit17Left� Top&WidthQHeightAutoSize	DataFieldData de nascimento
DataSourceDM1.DS3	MaxLength
TabOrder  TDBEditDBEdit18Left`TopFWidth� HeightAutoSize	DataFieldCPF ou CNPJ
DataSourceDM1.DS3	MaxLengthTabOrder  TDBEditDBEdit19LeftXTopfWidthJHeightAutoSize	DataField
Identidade
DataSourceDM1.DS3	MaxLengthTabOrder  TDBEditDBEdit20LeftPTopfWidthQHeightAutoSize	DataFieldData de expedi��o
DataSourceDM1.DS3	MaxLength
TabOrder  TDBEditDBEdit21LeftxTop� Width�HeightAutoSize	DataFieldOrg�o expeditor
DataSourceDM1.DS3TabOrder  TDBEditDBEdit22LeftPTop� Width	HeightAutoSize	DataFieldEndere�o
DataSourceDM1.DS3TabOrder  TDBEditDBEdit23LeftPTop� WidthiHeightAutoSize	DataField	Cidade/UF
DataSourceDM1.DS3TabOrder  TDBEditDBEdit24Left�Top� WidthaHeightAutoSize	DataFieldCEP
DataSourceDM1.DS3	MaxLengthTabOrder  TDBEditDBEdit25Left0Top� Width)HeightAutoSize	DataFieldPa�s
DataSourceDM1.DS3	MaxLength� TabOrder	  TDBEditDBEdit26LeftHTopWidthJHeightAutoSize	DataFieldTelefone
DataSourceDM1.DS3	MaxLengthTabOrder
  TDBEditDBEdit27Left� TopWidthJHeightAutoSize	DataFieldCelular
DataSourceDM1.DS3	MaxLengthTabOrder  TDBEditDBEdit28Left@Top&WidthHeightAutoSize	DataFieldE-mail
DataSourceDM1.DS3TabOrder  TDBMemoDBMemo1Left@TopFWidthHeight9	DataFieldPerfil
DataSourceDM1.DS3TabOrder   	TTabSheetTabFuncionarioCaptionFuncion�rioOnShowTabFuncionarioShow TLabelLabel15LeftTopWidth#HeightCaptionNome:FocusControlDBEdit15  TLabelLabel30LeftTop(Width� HeightCaptionData de Nascimento:FocusControlDBEdit29  TLabelLabel31LeftTopHWidthTHeightCaptionCPF ou CNPJ:FocusControlDBEdit30  TLabelLabel32LeftTophWidthMHeightCaptionIdentidade:FocusControlDBEdit31  TLabelLabel33Left� TophWidth~HeightCaptionData de Expedi��o:FocusControlDBEdit32  TLabelLabel34LeftTop� Width� HeightCaptionOrg�o do Expeditor:FocusControlDBEdit33  TLabelLabel35LeftTop� Width#HeightCaptionCTPS:FocusControlDBEdit34  TLabelLabel36Left� Top� Width� HeightCaptionFun��o/Ramo de Atividade:FocusControlDBEdit35  TLabelLabel37LeftTop� Width?HeightCaption	Endere�o:FocusControlDBEdit36  TLabelLabel38LeftTop� WidthFHeightCaption
Cidade/UF:FocusControlDBEdit37  TLabelLabel39Left�Top� WidthHeightCaptionCEP:FocusControlDBEdit38  TLabelLabel40LeftTopWidth#HeightCaptionPa�s:FocusControlDBEdit39  TLabelLabel41LeftTop(Width?HeightCaption	Telefone:FocusControlDBEdit40  TLabelLabel42Left� Top(Width8HeightCaptionCelular:FocusControlDBEdit41  TLabelLabel43LeftTopHWidth1HeightCaptionE-mail:FocusControlDBEdit42  TDBEditDBEdit15Left0TopWidth)HeightAutoSize	DataFieldNome
DataSourceDM1.DS4TabOrder 
OnKeyPressDBEdit15KeyPress  TDBEditDBEdit29Left� Top&WidthQHeightAutoSize	DataFieldData de nascimento
DataSourceDM1.DS4	MaxLength
TabOrder  TDBEditDBEdit30Left`TopFWidth� HeightAutoSize	DataFieldCPF ou CNPJ
DataSourceDM1.DS4	MaxLengthTabOrder  TDBEditDBEdit31LeftXTopfWidthJHeightAutoSize	DataField
Identidade
DataSourceDM1.DS4	MaxLengthTabOrder  TDBEditDBEdit32LeftXTopfWidthQHeightAutoSize	DataFieldData de expedi��o
DataSourceDM1.DS4	MaxLength
TabOrder  TDBEditDBEdit33Left� Top� Width�HeightAutoSize	DataFieldOrg�o do Expeditor
DataSourceDM1.DS4TabOrder  TDBEditDBEdit34Left0Top� WidthaHeightAutoSize	DataFieldCTPS
DataSourceDM1.DS4TabOrder  TDBEditDBEdit35LefthTop� Width� HeightAutoSize	DataFieldFun��o/Ramo de Atividade
DataSourceDM1.DS4TabOrder  TDBEditDBEdit36LeftHTop� WidthHeightAutoSize	DataFieldEndere�o
DataSourceDM1.DS4TabOrder  TDBEditDBEdit37LeftPTop� WidthiHeightAutoSize	DataField	Cidade/UF
DataSourceDM1.DS4TabOrder	  TDBEditDBEdit38Left�Top� WidthaHeightAutoSize	DataFieldCEP
DataSourceDM1.DS4	MaxLengthTabOrder
  TDBEditDBEdit39Left0TopWidth)HeightAutoSize	DataFieldPa�s
DataSourceDM1.DS4TabOrder  TDBEditDBEdit40LeftHTop&WidthJHeightAutoSize	DataFieldTelefone
DataSourceDM1.DS4	MaxLengthTabOrder  TDBEditDBEdit41Left� Top&WidthJHeightAutoSize	DataFieldCelular
DataSourceDM1.DS4	MaxLengthTabOrder  TDBEditDBEdit42Left@TopFWidthHeightAutoSize	DataFieldE-mail
DataSourceDM1.DS4TabOrder     TPanelPanel2Left Top�WidthsHeight)AlignalBottom
BevelOuterbvNoneParentColor	TabOrder TButtonOKBtnLeft� Top WidthiHeight#CaptionOKEnabledModalResultTabOrder OnClick
OKBtnClick  TButton	CancelBtnLeftHTop WidthiHeight#Cancel	CaptionCancelDefault	ModalResultTabOrderOnClickCancelBtnClick    