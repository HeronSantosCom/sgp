unit splash;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, jpeg, ExtCtrls;

type
  TSplashForm = class(TForm)
    Shape1: TShape;
    Image1: TImage;
    Animate1: TAnimate;
    Shape2: TShape;
    Image2: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SplashForm: TSplashForm;

implementation

uses login;

{$R *.DFM}


procedure TSplashForm.Timer1Timer(Sender: TObject);
begin
        LoginForm.show;
        SplashForm.hide;
        if timer1.enabled = true then
                timer1.enabled := false
end;

procedure TSplashForm.FormActivate(Sender: TObject);
begin
Animate1.Active:=True;
Timer1.Enabled:=True;
end;

end.
