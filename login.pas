unit login;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Db, DBTables, jpeg, ExtCtrls, Buttons;

type
  TLoginForm = class(TForm)
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    usuario: TEdit;
    senha: TEdit;
    Button1: TButton;
    Button2: TButton;
    Shape1: TShape;
    Image1: TImage;
    Shape2: TShape;
    Image2: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Button3: TBitBtn;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LoginForm: TLoginForm;

implementation

uses bd, main;

{$R *.DFM}

procedure TLoginForm.Button2Click(Sender: TObject);
begin
usuario.text:='';
senha.text:='';
usuario.setfocus;
end;

procedure TLoginForm.Button1Click(Sender: TObject);
var psw,user:string;
begin
user:=usuario.text;
psw:=senha.text;

if (user='') or (psw='') then
begin
        showmessage('Voc� precisa preencher os dados solicitados!')
end
else
if (user<>'') or (psw<>'') then
begin
        if not dm1.TUSER.FindKey([user]) then
        begin
                showmessage('Nome de usu�rio n�o cadastrado. Por Favor, Verifique!')
        end
        else
                if not dm1.TUSER.Locate('Senha',psw,[lopartialkey,locaseinsensitive]) then
                        begin
                        showmessage('Senha Incorreta. Por Favor, Verifique!')
                        end
                        else
                                begin
                                MainForm.Show;
                                LoginForm.Hide;
                                end;
end;
end;

procedure TLoginForm.Button3Click(Sender: TObject);
begin
halt;
end;

procedure TLoginForm.FormHide(Sender: TObject);
begin
DM1.TUSER.Active:=FALSE;
end;

procedure TLoginForm.FormShow(Sender: TObject);
begin
DM1.TUSER.Active:=True;
end;

end.
