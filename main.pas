unit main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ComCtrls, Buttons, StdCtrls, ExtCtrls, jpeg;

type
  TMainForm = class(TForm)
    StatusBar1: TStatusBar;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
    BitBtn13: TBitBtn;
    BitBtn17: TBitBtn;
    Shape2: TShape;
    Shape5: TShape;
    Shape8: TShape;
    Image1: TImage;
    Shape3: TShape;
    SpeedButton1: TSpeedButton;
    Shape4: TShape;
    Timer1: TTimer;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Shape1: TShape;
    Shape6: TShape;
    procedure Sobre1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure BitBtn17Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

uses about, cadastro, bd, procura, financeira, admin;

{$R *.DFM}

procedure TMainForm.Sobre1Click(Sender: TObject);
begin
AboutForm.Show;
end;

procedure TMainForm.BitBtn1Click(Sender: TObject);
begin
halt;
end;

procedure TMainForm.Timer1Timer(Sender: TObject);
var
Ano,Mes,Dia:Word;
Hora,Min,Sec,MSec:Word;
begin

  DecodeDate(Now,Ano,Mes,Dia);
  DecodeTime(Now,Hora,Min,Sec,MSec);
  ShortDateFormat := 'dd/mm/yyyy';
  LongTimeFormat  := 'hh:nn';
  LongDayNames[1] := 'Domingo'; LongDayNames[2] := 'Segunda-feira'; LongDayNames[3] := 'Ter�a-feira';
  LongDayNames[4] := 'Quarta-feira';  LongDayNames[5] := 'Quinta-feira';  LongDayNames[6] := 'Sexta-feira';
  LongDayNames[7] := 'S�bado';
  statusbar1.Panels[0].text  := FormatDateTime('dddd, c',
                                     StrToDateTime(IntToStr(Dia) + '/' + IntToStr(Mes)  + '/' +
                                                   IntToStr(Ano) + ' ' + IntToStr(Hora) + ':' +
                                                   IntToStr(Min) + ':' + IntToStr(Sec)));
end;

procedure TMainForm.FormActivate(Sender: TObject);
begin
Timer1.Enabled:=True;
end;

procedure TMainForm.BitBtn2Click(Sender: TObject);
begin
CadastroForm.Show;
CadastroForm.PageControl1.ActivePageIndex:=0;
CadastroForm.TabTalento.Show;
end;

procedure TMainForm.BitBtn3Click(Sender: TObject);
begin
CadastroForm.Show;
CadastroForm.PageControl1.ActivePageIndex:=1;
CadastroForm.TabCliente.Show;
end;

procedure TMainForm.BitBtn4Click(Sender: TObject);
begin
CadastroForm.Show;
CadastroForm.PageControl1.ActivePageIndex:=2;
CadastroForm.TabFuncionario.Show;
end;

procedure TMainForm.SpeedButton1Click(Sender: TObject);
begin
AboutForm.show;
end;

procedure TMainForm.BitBtn11Click(Sender: TObject);
begin
ProcuraForm.Show;
ProcuraForm.PageControl1.ActivePageIndex:=0;
ProcuraForm.TabTalento.Show;
end;

procedure TMainForm.BitBtn12Click(Sender: TObject);
begin
ProcuraForm.Show;
ProcuraForm.PageControl1.ActivePageIndex:=1;
ProcuraForm.TabCliente.Show;
end;

procedure TMainForm.BitBtn13Click(Sender: TObject);
begin
ProcuraForm.Show;
ProcuraForm.PageControl1.ActivePageIndex:=2;
ProcuraForm.TabFuncionario.Show;
end;

procedure TMainForm.BitBtn14Click(Sender: TObject);
begin
FinanceiraForm.Show;
end;

procedure TMainForm.BitBtn17Click(Sender: TObject);
begin
Beep;
MessageDlg('Consulte o manual antes de manusear esta �rea!',mtWarning,[mbOk],0);
AdminForm.Show;
end;

end.
