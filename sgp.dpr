program sgp;

uses
  Forms,
  splash in 'splash.pas' {SplashForm},
  about in 'about.pas' {AboutForm},
  login in 'login.pas' {LoginForm},
  bd in 'bd.pas' {DM1: TDataModule},
  main in 'main.pas' {MainForm},
  cadastro in 'cadastro.pas' {CadastroForm},
  procura in 'procura.pas' {ProcuraForm},
  admin in 'admin.pas' {AdminForm};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'SGP - Sistema de Gerenciamento Publicitário';
  Application.CreateForm(TSplashForm, SplashForm);
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TLoginForm, LoginForm);
  Application.CreateForm(TAboutForm, AboutForm);
  Application.CreateForm(TDM1, DM1);
  Application.CreateForm(TCadastroForm, CadastroForm);
  Application.CreateForm(TProcuraForm, ProcuraForm);
  Application.CreateForm(TAdminForm, AdminForm);
  Application.Run;
end.
