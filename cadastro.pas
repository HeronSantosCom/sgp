unit cadastro;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ComCtrls, ExtCtrls, Mask, DBCtrls;

type
  TCadastroForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabTalento: TTabSheet;
    TabCliente: TTabSheet;
    TabFuncionario: TTabSheet;
    OKBtn: TButton;
    CancelBtn: TButton;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    Label17: TLabel;
    DBEdit17: TDBEdit;
    Label18: TLabel;
    DBEdit18: TDBEdit;
    Label19: TLabel;
    DBEdit19: TDBEdit;
    Label20: TLabel;
    DBEdit20: TDBEdit;
    Label21: TLabel;
    DBEdit21: TDBEdit;
    Label22: TLabel;
    DBEdit22: TDBEdit;
    Label23: TLabel;
    DBEdit23: TDBEdit;
    Label24: TLabel;
    DBEdit24: TDBEdit;
    Label25: TLabel;
    DBEdit25: TDBEdit;
    Label26: TLabel;
    DBEdit26: TDBEdit;
    Label27: TLabel;
    DBEdit27: TDBEdit;
    Label28: TLabel;
    DBEdit28: TDBEdit;
    Label29: TLabel;
    DBMemo1: TDBMemo;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    Label30: TLabel;
    DBEdit29: TDBEdit;
    Label31: TLabel;
    DBEdit30: TDBEdit;
    Label32: TLabel;
    DBEdit31: TDBEdit;
    Label33: TLabel;
    DBEdit32: TDBEdit;
    Label34: TLabel;
    DBEdit33: TDBEdit;
    Label35: TLabel;
    DBEdit34: TDBEdit;
    Label36: TLabel;
    DBEdit35: TDBEdit;
    Label37: TLabel;
    DBEdit36: TDBEdit;
    Label38: TLabel;
    DBEdit37: TDBEdit;
    Label39: TLabel;
    DBEdit38: TDBEdit;
    Label40: TLabel;
    DBEdit39: TDBEdit;
    Label41: TLabel;
    DBEdit40: TDBEdit;
    Label42: TLabel;
    DBEdit41: TDBEdit;
    Label43: TLabel;
    DBEdit42: TDBEdit;
    procedure CancelBtnClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure TabTalentoShow(Sender: TObject);
    procedure TabClienteShow(Sender: TObject);
    procedure TabFuncionarioShow(Sender: TObject);
    procedure DBEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure DBEdit16KeyPress(Sender: TObject; var Key: Char);
    procedure DBEdit15KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CadastroForm: TCadastroForm;

implementation

uses bd;

{$R *.DFM}

procedure TCadastroForm.CancelBtnClick(Sender: TObject);
var n,n2:integer;
begin
N:=PageControl1.ActivePageIndex;
n2:=n;
Case N OF
0: DM1.TCACATALENTO.CANCEL;
1: DM1.TCLIENTE.CANCEL;
2: DM1.TFUNCIONARIO.CANCEL;
END;
Case n2 OF
0: TabTalento.Hide;
1: TabCliente.Hide;
2: TabFuncionario.Hide;
END;
TabTalento.Hide;
CadastroForm.Hide;
end;


procedure TCadastroForm.OKBtnClick(Sender: TObject);
var n,n2:integer;
begin
N:=PageControl1.ActivePageIndex;
n2:=n;
Case N OF
0: DM1.TCACATALENTO.POST;
1: DM1.TCLIENTE.POST;
2: DM1.TFUNCIONARIO.POST;
END;
Case n2 OF
0: TabTalento.Hide;
1: TabCliente.Hide;
2: TabFuncionario.Hide;
END;
TabTalento.Hide;
CadastroForm.Hide;
end;

procedure TCadastroForm.TabTalentoShow(Sender: TObject);
begin
DM1.TCACATALENTO.Active:=True;
DM1.TCACATALENTO.Append;
DBEdit1.SetFocus;
end;


procedure TCadastroForm.TabClienteShow(Sender: TObject);
begin
DM1.TCLIENTE.Active:=True;
DM1.TCLIENTE.Append;
DBEdit16.SetFocus;
end;

procedure TCadastroForm.TabFuncionarioShow(Sender: TObject);
begin
DM1.TFUNCIONARIO.Active:=True;
DM1.TFUNCIONARIO.Append;
DBEdit15.SetFocus;
end;

procedure TCadastroForm.DBEdit1KeyPress(Sender: TObject; var Key: Char);
begin
If (key in ['A'..'B','a'..'b', #8]) then
begin
OKBtn.Enabled:=false;
CancelBtn.Default:=True;
end
else
begin
OKBtn.Enabled:=True;
OKBtn.Default:=True;
end;
end;

procedure TCadastroForm.DBEdit16KeyPress(Sender: TObject; var Key: Char);
begin
If (key in ['A'..'B','a'..'b', #8]) then
begin
OKBtn.Enabled:=false;
CancelBtn.Default:=True;
end
else
begin
OKBtn.Enabled:=True;
OKBtn.Default:=True;
end;
end;

procedure TCadastroForm.DBEdit15KeyPress(Sender: TObject; var Key: Char);
begin
If (key in ['A'..'B','a'..'b', #8]) then
begin
OKBtn.Enabled:=false;
CancelBtn.Default:=True;
end
else
begin
OKBtn.Enabled:=True;
OKBtn.Default:=True;
end;
end;

end.

