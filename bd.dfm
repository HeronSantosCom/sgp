object DM1: TDM1
  OldCreateOrder = False
  Left = 228
  Top = 398
  Height = 480
  Width = 696
  object DS1: TDataSource
    DataSet = TUSER
    Left = 24
    Top = 8
  end
  object TUSER: TTable
    DatabaseName = 'C:\Arquivos de programas\Eclipse\SGP'
    TableName = 'usuario.DB'
    Left = 80
    Top = 8
    object TUSERUsuario: TStringField
      FieldName = 'Usuario'
    end
    object TUSERSenha: TStringField
      FieldName = 'Senha'
    end
    object TUSERFuncao: TStringField
      FieldName = 'Funcao'
      Size = 15
    end
  end
  object DS2: TDataSource
    DataSet = TCACATALENTO
    Left = 24
    Top = 56
  end
  object TCACATALENTO: TTable
    DatabaseName = 'C:\Arquivos de programas\Eclipse\SGP'
    TableName = 'cacatalento.db'
    Left = 80
    Top = 56
    object TCACATALENTOCodigo: TAutoIncField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TCACATALENTONome: TStringField
      FieldName = 'Nome'
      Size = 255
    end
    object TCACATALENTODatadenascimento: TDateField
      FieldName = 'Data de nascimento'
    end
    object TCACATALENTOCPFouCNPJ: TFloatField
      FieldName = 'CPF ou CNPJ'
    end
    object TCACATALENTORazosocial: TStringField
      FieldName = 'Raz�o social'
      Size = 255
    end
    object TCACATALENTOIdentidade: TFloatField
      FieldName = 'Identidade'
    end
    object TCACATALENTODatadeexpedio: TDateField
      FieldName = 'Data de expedi��o'
    end
    object TCACATALENTOOrgoexpeditor: TStringField
      FieldName = 'Org�o expeditor'
      Size = 255
    end
    object TCACATALENTOEndereo: TStringField
      FieldName = 'Endere�o'
      Size = 255
    end
    object TCACATALENTOCidadeUF: TStringField
      FieldName = 'Cidade/UF'
      Size = 50
    end
    object TCACATALENTOCEP: TFloatField
      FieldName = 'CEP'
    end
    object TCACATALENTOPas: TStringField
      FieldName = 'Pa�s'
      Size = 50
    end
    object TCACATALENTOTelefone: TFloatField
      FieldName = 'Telefone'
    end
    object TCACATALENTOCelular: TFloatField
      FieldName = 'Celular'
    end
    object TCACATALENTOEmail: TStringField
      FieldName = 'E-mail'
      Size = 255
    end
  end
  object DS3: TDataSource
    DataSet = TCLIENTE
    Left = 24
    Top = 112
  end
  object DS4: TDataSource
    DataSet = TFUNCIONARIO
    Left = 24
    Top = 168
  end
  object TCLIENTE: TTable
    DatabaseName = 'C:\Arquivos de programas\Eclipse\SGP'
    TableName = 'cliente.db'
    Left = 80
    Top = 112
    object TCLIENTECodigo: TAutoIncField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TCLIENTENome: TStringField
      FieldName = 'Nome'
      Size = 255
    end
    object TCLIENTEDatadenascimento: TDateField
      FieldName = 'Data de nascimento'
    end
    object TCLIENTECPFouCNPJ: TFloatField
      FieldName = 'CPF ou CNPJ'
    end
    object TCLIENTEIdentidade: TFloatField
      FieldName = 'Identidade'
    end
    object TCLIENTEDatadeexpedio: TDateField
      FieldName = 'Data de expedi��o'
    end
    object TCLIENTEOrgoexpeditor: TStringField
      FieldName = 'Org�o expeditor'
      Size = 255
    end
    object TCLIENTEEndereo: TStringField
      FieldName = 'Endere�o'
      Size = 255
    end
    object TCLIENTECidadeUF: TStringField
      FieldName = 'Cidade/UF'
      Size = 50
    end
    object TCLIENTECEP: TFloatField
      FieldName = 'CEP'
    end
    object TCLIENTEPas: TStringField
      FieldName = 'Pa�s'
      Size = 50
    end
    object TCLIENTETelefone: TFloatField
      FieldName = 'Telefone'
    end
    object TCLIENTECelular: TFloatField
      FieldName = 'Celular'
    end
    object TCLIENTEEmail: TStringField
      FieldName = 'E-mail'
      Size = 255
    end
    object TCLIENTEPerfil: TMemoField
      FieldName = 'Perfil'
      BlobType = ftMemo
      Size = 240
    end
  end
  object TFUNCIONARIO: TTable
    DatabaseName = 'C:\Arquivos de programas\Eclipse\SGP'
    TableName = 'funcionario.db'
    Left = 80
    Top = 168
    object TFUNCIONARIOCodigo: TAutoIncField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TFUNCIONARIONome: TStringField
      FieldName = 'Nome'
      Size = 255
    end
    object TFUNCIONARIODatadenascimento: TDateField
      FieldName = 'Data de nascimento'
    end
    object TFUNCIONARIOCPFouCNPJ: TFloatField
      FieldName = 'CPF ou CNPJ'
    end
    object TFUNCIONARIOIdentidade: TFloatField
      FieldName = 'Identidade'
    end
    object TFUNCIONARIODatadeexpedio: TDateField
      FieldName = 'Data de expedi��o'
    end
    object TFUNCIONARIOOrgodoExpeditor: TStringField
      FieldName = 'Org�o do Expeditor'
      Size = 255
    end
    object TFUNCIONARIOCTPS: TFloatField
      FieldName = 'CTPS'
    end
    object TFUNCIONARIOFunoRamodeAtividade: TStringField
      FieldName = 'Fun��o/Ramo de Atividade'
      Size = 255
    end
    object TFUNCIONARIOEndereo: TStringField
      FieldName = 'Endere�o'
      Size = 255
    end
    object TFUNCIONARIOCidadeUF: TStringField
      FieldName = 'Cidade/UF'
      Size = 50
    end
    object TFUNCIONARIOCEP: TFloatField
      FieldName = 'CEP'
    end
    object TFUNCIONARIOPas: TStringField
      FieldName = 'Pa�s'
      Size = 255
    end
    object TFUNCIONARIOTelefone: TFloatField
      FieldName = 'Telefone'
    end
    object TFUNCIONARIOCelular: TFloatField
      FieldName = 'Celular'
    end
    object TFUNCIONARIOEmail: TStringField
      FieldName = 'E-mail'
      Size = 255
    end
  end
end
