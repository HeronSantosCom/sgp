unit bd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables;

type
  TDM1 = class(TDataModule)
    DS1: TDataSource;
    TUSER: TTable;
    TUSERUsuario: TStringField;
    TUSERSenha: TStringField;
    TUSERFuncao: TStringField;
    DS2: TDataSource;
    TCACATALENTO: TTable;
    DS3: TDataSource;
    DS4: TDataSource;
    TCLIENTE: TTable;
    TFUNCIONARIO: TTable;
    TCACATALENTOCodigo: TAutoIncField;
    TCACATALENTONome: TStringField;
    TCACATALENTODatadenascimento: TDateField;
    TCACATALENTOCPFouCNPJ: TFloatField;
    TCACATALENTORazosocial: TStringField;
    TCACATALENTOIdentidade: TFloatField;
    TCACATALENTODatadeexpedio: TDateField;
    TCACATALENTOOrgoexpeditor: TStringField;
    TCACATALENTOEndereo: TStringField;
    TCACATALENTOCidadeUF: TStringField;
    TCACATALENTOCEP: TFloatField;
    TCACATALENTOPas: TStringField;
    TCACATALENTOTelefone: TFloatField;
    TCACATALENTOCelular: TFloatField;
    TCACATALENTOEmail: TStringField;
    TCLIENTECodigo: TAutoIncField;
    TCLIENTENome: TStringField;
    TCLIENTEDatadenascimento: TDateField;
    TCLIENTECPFouCNPJ: TFloatField;
    TCLIENTEIdentidade: TFloatField;
    TCLIENTEDatadeexpedio: TDateField;
    TCLIENTEOrgoexpeditor: TStringField;
    TCLIENTEEndereo: TStringField;
    TCLIENTECidadeUF: TStringField;
    TCLIENTECEP: TFloatField;
    TCLIENTEPas: TStringField;
    TCLIENTETelefone: TFloatField;
    TCLIENTECelular: TFloatField;
    TCLIENTEEmail: TStringField;
    TCLIENTEPerfil: TMemoField;
    TFUNCIONARIOCodigo: TAutoIncField;
    TFUNCIONARIONome: TStringField;
    TFUNCIONARIODatadenascimento: TDateField;
    TFUNCIONARIOCPFouCNPJ: TFloatField;
    TFUNCIONARIOIdentidade: TFloatField;
    TFUNCIONARIODatadeexpedio: TDateField;
    TFUNCIONARIOOrgodoExpeditor: TStringField;
    TFUNCIONARIOCTPS: TFloatField;
    TFUNCIONARIOFunoRamodeAtividade: TStringField;
    TFUNCIONARIOEndereo: TStringField;
    TFUNCIONARIOCidadeUF: TStringField;
    TFUNCIONARIOCEP: TFloatField;
    TFUNCIONARIOPas: TStringField;
    TFUNCIONARIOTelefone: TFloatField;
    TFUNCIONARIOCelular: TFloatField;
    TFUNCIONARIOEmail: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM1: TDM1;

implementation

{$R *.DFM}

end.
